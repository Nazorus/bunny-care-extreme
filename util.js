function clamp(min, number, max) {
  return Math.min(Math.max(number, min), max);
}

function rgbToHex(r, g, b) {
  return "0x" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}