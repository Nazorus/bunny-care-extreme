class Game {
  // pixi app
  app;
  loader;

  // visual layers
  foreground;
  middleground;
  background;

  // user interface
  gui;
  actionIcon;
  crosshairIcon;
  redCrosshairIcon;

  // environment
  floor;

  // actors
  rabbit;
  zombies;

  // player
  gun;
  gunTimer = 0;

  // game state
  started = false;
  lost = false;
  time = 0;

  // loading screen
  loading;

  init() {
    // use webgl if available
    let type = "WebGL";
    if(!PIXI.utils.isWebGLSupported()){
      type = "canvas";
    }
  
    // say hi
    PIXI.utils.sayHello(type);
  
    // create pixi app
    this.app = new PIXI.Application({width: GAME_WIDTH, height: GAME_HEIGHT});
    this.app.renderer.backgroundColor = BACKGROUND_COLOR;

    // show loading screen
    this.loading = new PIXI.Text("LOADING", { fontFamily: 'Courier New, Courier, monospace', fontWeight: 'bolder', fontSize: 54, fill: 0xffffff, align: 'center' });
    this.loading.anchor.set(0.5);
    this.loading.x = GAME_WIDTH / 2,
    this.loading.y = GAME_HEIGHT / 2 - 64;
    this.loading.visible = true;
    this.app.stage.addChild(this.loading);
  
    // add Pixi canvas to HTML document
    document.getElementById('game').appendChild(this.app.view);

    // use custom mouse cursor
    this.actionIcon = "url('assets/action.png') 8 0, pointer";
    this.crosshairIcon = "url('assets/crosshair.png') 16 16, crosshair";
    this.redCrosshairIcon = "url('assets/crosshair_red.png') 16 16, crosshair";

    this.app.renderer.plugins.interaction.cursorStyles.default = this.actionIcon;
    this.app.renderer.plugins.interaction.cursorStyles.hover = this.actionIcon;
    this.app.renderer.plugins.interaction.cursorStyles.move = this.redCrosshairIcon;

    // add foreground and background layers
    this.background = new PIXI.Container();
    this.app.stage.addChild(this.background);

    this.middleground = new PIXI.Container();
    this.middleground.sortableChildren = true;
    this.app.stage.addChild(this.middleground);

    this.foreground = new PIXI.Container();
    this.app.stage.addChild(this.foreground);

    // draw the floor
    this.floor = new PIXI.Graphics();
    this.floor.beginFill(0x724216);
    this.floor.lineStyle(10, 0x523208);
    this.floor.drawRect(-20, GAME_HEIGHT - 134, GAME_WIDTH + 40, 200);
    this.floor.tint = 0xAAAAAA;

    this.background.addChild(this.floor);

    // load assets
    this.loader = new PIXI.Loader();
    this.loader
      // add sprites
      .add([BUTTON_PNG, RABBIT_PNG, ZOMBIE1_PNG, ZOMBIE2_PNG, CROSSHAIR_PNG, FOOD_PNG, WATER_PNG, PET_PNG, SHOOTING_PNG, GUN_PNG])
      // add sound effects
      .add([SHOT_OGG, OUCH_OGG, KILL_OGG, ZOMBIE1_OGG, ZOMBIE2_OGG, FOOD_OGG, WATER_OGG, STRESS_OGG])
      // add music
      .add([BASS_OGG, RIFF_OGG, FULL_OGG, END_OGG])
      .load(() => {      
        // add gui
        this.gui = new GUI(this, this.foreground, this.loader);
  
        // add zombie manager
        this.zombies = new Zombies(
          this,
          this.loader.resources[ZOMBIE1_PNG].texture,
          this.loader.resources[ZOMBIE2_PNG].texture
        );
         
        // add rabbit    
        this.rabbit = new Rabbit(
          this,
          this.loader.resources[RABBIT_PNG].texture
        );

        // add gun sprite
        this.gun = new PIXI.AnimatedSprite([
          this.loader.resources[SHOOTING_PNG].texture,
          this.loader.resources[GUN_PNG].texture
        ]);

        // position and visibiliy
        this.gun.anchor.set(0.5);
        this.gun.y = GAME_HEIGHT - this.gun.height / 2;
        this.gun.visible = false;

        // animation config
        this.gun.loop = false;
        this.gun.animationSpeed = GAME_SPEED / 10;

        this.foreground.addChild(this.gun);

        // start ticker
        this.app.ticker.add(delta => this.gameLoop(delta));

        // set music parameters
        const bassLoop = this.loader.resources[BASS_OGG].sound; 
        bassLoop.loop = true;
        bassLoop.volume = 0.1;
        
        const fullLoop = this.loader.resources[FULL_OGG].sound;
        fullLoop.loop = true;
        fullLoop.volume = 0.1;

        this.loader.resources[END_OGG].sound.volume = 0.2;

        // finished loading
        this.loading.visible = false;
      });
  }

  hello() {
    // display instructions
    document.getElementById('needs').style.display = "block";
    document.getElementById('destroy').style.display = "block";

    // play bass loop
    this.playSound(BASS_OGG);
  }

  start() {
    // change default icon to crosshair
    this.app.renderer.plugins.interaction.cursorStyles.default = this.crosshairIcon;

    // reset game state
    this.time = 0;
    this.lost = false;
    this.started = true;

    // reset background
    this.app.renderer.backgroundColor = BACKGROUND_COLOR;
    
    // reset music
    this.stopSound(END_OGG);
    this.stopSound(BASS_OGG);
    this.playSound(FULL_OGG);

    // reset actors
    this.rabbit.reset();
    this.zombies.clear();
  }

  lose() {
    // only do once
    if (!this.lost) {
      this.stopSound(FULL_OGG);
      this.playSound(END_OGG);
      this.playSound(BASS_OGG);
      this.lost = true;
      this.gui.lose(this.rabbit.causeOfDeath);
    }

    // every fraw the game is "lost"
    this.app.renderer.backgroundColor = LOST_BACKGROUND_COLOR;
  }

  gameLoop(deltaTime) {
    // do nothing before the game starts
    if (!this.started) {
      return;
    }

    // apply game speed
    deltaTime *= GAME_SPEED;

    if (!this.lost) {
      // only count timer if not lost
      this.time += this.app.ticker.deltaMS;

      // update gui timer
      this.gui.updateTimer(this.time);  
      
      // refresh rabbit health
      this.rabbit.refresh(deltaTime);

      // check if a zombie killed the rabbit
      this.zombies.zombies.forEach(zombie => {
        if (this.spritesCollide(this.rabbit.sprite, zombie.sprite)) {
          this.rabbit.die('ZOMBIFICATION');
        }
      });
    }

    // spawn zombies after a few seconds
    if (this.time > 5_000) {
      // difficulty modifier
      // cannot go above 3, is at maximum at 2 minutes
      const difficultyModifier = Math.min(3, this.time / 40_000);
      
      // 0.20% to 0.80% chance to spawn a zombie every frame
      // do not allow more than 4 to 7 zombies on screen
      if(Math.random() < deltaTime * (1 + difficultyModifier) / 500 && this.zombies.count < 4 + difficultyModifier) {
        // spawn a zombie with adaptive speed
        this.zombies.spawnZombie(difficultyModifier, GAME_SPEED);
      }
    }

    // refresh zombie manager
    this.zombies.refresh(deltaTime);

    // gun animation
    if (this.gun.visible) {
      this.gunTimer -= this.app.ticker.deltaMS;

      if (this.gunTimer <= 0) {
        this.gun.visible = false;
        this.gunTimer = 0;
      }
    }

    // check rabbit health
    if (!this.rabbit.alive) {
      this.lose();
    }
  }

  // bunny care
  giveFood() {
    this.rabbit.giveFood();
  }

  giveWater() {
    this.rabbit.giveWater();
  }

  givePet() {
    this.rabbit.givePet();
  }

  // shooting
  shoot(x) {
    // flash background and floor
    this.app.renderer.backgroundColor = BACKGROUND_COLOR + 0x444444;
    this.floor.tint = 0xFFFFFF;
    
    // only for 200ms
    setTimeout(() => {
      this.app.renderer.backgroundColor = BACKGROUND_COLOR;
      this.floor.tint = 0xAAAAAA;
    }, 200);

    // show gun
    this.gun.x = x;
    this.gun.visible = true;
    this.gunTimer = 800;
    
    // animate
    this.gun.gotoAndPlay(0);
  
    // play shot sound
    this.playSound(SHOT_OGG);
  }

  // physics
  spritesCollide(spriteA, spriteB) {
    // calculate distance between sprites
    // with margin because hitboxes are bad
    const distanceX = Math.abs(spriteA.x - spriteB.x) + 20;
    const distanceY = Math.abs(spriteA.y - spriteB.y) + 20;
  
    // calculate combined half-widths and half-heights
    const combinedHalfWidths = spriteA.width / 2 + spriteB.width / 2;
    const combinedHalfHeights = spriteA.height / 2 + spriteB.height / 2;
  
    // check if overlapping on x or y
    return distanceX < combinedHalfWidths && distanceY < combinedHalfHeights;
  }

  // sounds
  playSound(id) {
    this.loader.resources[id].sound.play();
  }

  stopSound(id) {
    this.loader.resources[id].sound.stop();
  }

  get foregroundLayer() {
    return this.foreground;
  }

  get middlegroundLayer() {
    return this.middleground;
  }

  get backgroundLayer() {
    return this.background;
  }

  get renderer() {
    return this.app.renderer;
  }
}