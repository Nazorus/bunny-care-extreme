class GUI {
  // game
  game;

  // game messages
  message = 'DEATH BY ';
  messageSprite;

  // start buttons
  helloSprite;
  helloText;
  helloBorder;

  startSprite;
  startText;
  startBorder;
  
  // bunny care buttons
  foodSprite;
  foodBorder;

  waterSprite;
  waterBorder;

  petSprite;
  petBorder;

  // timer
  timerSprite;

  // text config
  buttonTextConfig = { fontFamily: 'Courier New, Courier, monospace', fontWeight: 'bolder', fontSize: 42, fill: 0xffffff, align: 'center' };
  timerTextConfig = { fontFamily: 'Courier New, Courier, monospace', fontWeight: 'bolder', fontSize: 54, fill: 0xffffff, align: 'center' };
  messageTextConfig = { fontFamily: 'Courier New, Courier, monospace', fontWeight: 'bolder', fontSize: 48, fill: 0xffffff, align: 'center' };

  constructor(game, stage, loader) {
    this.game = game;

    // add hello button
    this.helloSprite = new PIXI.Sprite(loader.resources[BUTTON_PNG].texture);
    this.helloBorder = new PIXI.Graphics();

    // setup
    this.setupButton(
      stage,
      this.helloSprite,
      this.helloBorder,
      GAME_WIDTH / 2,
      GAME_HEIGHT / 2 - 64,
      true
    );

    // add hello text
    this.helloText = new PIXI.Text("HELLO", this.buttonTextConfig);
    this.helloText.anchor.set(0.5);
    this.helloText.x = this.helloSprite.x;
    this.helloText.y = this.helloSprite.y;
    this.helloText.visible = true;

    stage.addChild(this.helloText);

    // hello on click
    this.helloSprite.on('mousedown', () => {
      this.hello();
    });

    // add start button
    this.startSprite = new PIXI.Sprite(loader.resources[BUTTON_PNG].texture);
    this.startBorder = new PIXI.Graphics();

    // setup
    this.setupButton(
      stage,
      this.startSprite,
      this.startBorder,
      GAME_WIDTH / 2,
      GAME_HEIGHT / 2 - 64,
      false
    );

    // add start text
    this.startText = new PIXI.Text("START", this.buttonTextConfig);
    this.startText.anchor.set(0.5);
    this.startText.x = this.startSprite.x;
    this.startText.y = this.startSprite.y;
    this.startText.visible = false;

    stage.addChild(this.startText);
    
    // start on click
    this.startSprite.on('mousedown', () => {
      this.start();
    });

    // add timer
    this.timerSprite = new PIXI.Text("0:00", this.timerTextConfig);
    this.timerSprite.anchor.set(0.5);
    this.timerSprite.x = GAME_WIDTH / 2;
    this.timerSprite.y = 64;
    this.timerSprite.visible = false;

    stage.addChild(this.timerSprite);

    // add game messages
    this.messageSprite = new PIXI.Text(this.message, this.messageTextConfig);
    this.messageSprite.anchor.set(0.5);
    this.messageSprite.x = GAME_WIDTH / 2;
    this.messageSprite.y = GAME_HEIGHT / 2;
    this.messageSprite.visible = false;

    stage.addChild(this.messageSprite);

    // food button
    this.foodSprite = new PIXI.Sprite(loader.resources[FOOD_PNG].texture);
    this.foodBorder = new PIXI.Graphics();

    // setup
    this.setupButton(
      stage,
      this.foodSprite,
      this.foodBorder,
      GAME_WIDTH / 2 - 128,
      GAME_HEIGHT / 2 - 64,
      false
    );

    // interactivity
    this.foodSprite
      .on('mousedown', () => {
        this.game.giveFood();
      })

    // water button
    this.waterSprite = new PIXI.Sprite(loader.resources[WATER_PNG].texture);
    this.waterBorder = new PIXI.Graphics();
    
    // setup
    this.setupButton(
      stage,
      this.waterSprite,
      this.waterBorder,
      GAME_WIDTH / 2,
      GAME_HEIGHT / 2 - 64,
      false
    );

    // interactivity
    this.waterSprite.interactive = true;
    this.waterSprite.on('mousedown', () => {
      this.game.giveWater();
    });
    
    this.petSprite = new PIXI.Sprite(loader.resources[PET_PNG].texture);
    this.petBorder = new PIXI.Graphics();
    
    // setup
    this.setupButton(
      stage,
      this.petSprite,
      this.petBorder,
      GAME_WIDTH / 2 + 128,
      GAME_HEIGHT / 2 - 64,
      false
    );

    // interactivity, feed on click
    this.petSprite.interactive = true;
    this.petSprite.on('mousedown', () => {
      this.game.givePet();
    });
  }

  setupButton(stage, sprite, border, x, y, visible) {
    sprite.anchor.set(0.5);
    sprite.x = x;
    sprite.y = y;
    sprite.visible = visible;
    sprite.cursor = 'hover';

    // draw borders
    border.lineStyle(8, 0xFFFFFF);
    border.drawRect(
      sprite.x - sprite.width / 2,
      sprite.y - sprite.height / 2,
      sprite.width,
      sprite.height
    );
    border.tint = 0x000000;
    border.visible = visible;


    // interactivity
    sprite.interactive = true;
    sprite
      .on('pointerover', () => {
        border.tint = 0xFFFFFF;
      })
      .on('pointerout', () => {
        border.tint = 0x000000;
      });
    
    stage.addChild(sprite);
    stage.addChild(border);
  }

  hello() {
    this.game.hello();

    this.helloSprite.visible = false;
    this.helloBorder.visible = false;
    this.helloText.visible = false;

    this.startSprite.visible = true;
    this.startBorder.visible = true;
    this.startText.visible = true;

    this.timerSprite.visible = true;
  }

  start() {
    this.game.start();

    // hide pre-game and lose buttons
    this.startSprite.visible = false;
    this.startBorder.visible = false;
    this.startText.visible = false;
    this.messageSprite.visible = false;

    // show game buttons
    this.foodSprite.visible = true;
    this.foodBorder.visible = true;

    this.waterSprite.visible = true;
    this.waterBorder.visible = true;

    this.petSprite.visible = true;
    this.petBorder.visible = true;
  }

  lose(cause) {
    // update cause of death
    this.messageSprite.text = this.message + cause;

    // show lose buttons
    this.startSprite.visible = true;
    this.startBorder.visible = true;

    this.startText.text = "RETRY";
    this.startText.visible = true;
    
    this.messageSprite.visible = true;
  
    // hide game buttons
    this.foodSprite.visible = false;
    this.foodBorder.visible = false;

    this.waterSprite.visible = false;
    this.waterBorder.visible = false;

    this.petSprite.visible = false;
    this.petBorder.visible = false;
  }

  updateTimer(time) {
    // seconds
    const currentSeconds = Math.floor(time / 1000);

    // minutes
    const minutes = Math.floor(currentSeconds / 60).toString();

    // seconds
    const secondsString = (currentSeconds % 60).toString();
    const seconds = secondsString.length === 1 ? '0' + secondsString : secondsString;

    this.timerSprite.text = minutes + ':' + seconds;
  }
}