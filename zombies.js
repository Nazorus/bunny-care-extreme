class Zombies {
  constructor(game, texture1, texture2) {
    // game
    this._game = game;

    // sound and sprite
    this._texture1 = texture1;
    this._texture2 = texture2;
    
    // zombie list
    this._zombies = [];
  }

  get zombies() {
    return this._zombies;
  }

  get count() {
    return this._zombies.length;
  }

  spawnZombie(speedModifier) {
    // choose a random side to spawn at
    let side;
    const random = Math.random() * 2;
    if (random >= 1) {
      side = 1;
    }
    else {
      side = -1;
    }

    // make zombie object
    const zombie = new Zombie(
      this._game,
      this._texture1,
      this._texture2,
      side,
      speedModifier
    );
    
    // add zombie to list
    this._zombies.push(zombie);

    // play zombie spawn sound
    const randomSound = Math.random() * 2;
    this._game.playSound(randomSound >= 1 ? ZOMBIE1_OGG : ZOMBIE2_OGG);
  }

  clear() {
    // destroy all zombies
    this._zombies.forEach(zombie => {
      zombie.destroy();
    });

    // clear list
    this._zombies = [];
  }

  refresh(deltaTime) {
    // kill zombies that went too far off screen
    this._zombies.forEach(zombie => {
      if (zombie.sprite.x > GAME_WIDTH + 128 || zombie.sprite.x < -128) {
        zombie.dead = true;
      }
    });

    // destroy dead zombies
    this._zombies.forEach(zombie => {
      if(zombie.dead) {
        zombie.destroy();
      }
    });

    // remove dead zombies from list
    this._zombies = this._zombies.filter(zombie => {
      return !zombie.dead;
    });

    // move zombies that are still alive
    this._zombies.forEach(zombie => {
      zombie.move(deltaTime);
    });
  }
}

class Zombie {  
  constructor(game, texture1, texture2, side, speedModifier) {   
    this._game = game;
    
    // game properties
    this._dead = false;
    this._side = side;
    this._speed =  0.5 + speedModifier / 2 + Math.random();

    // make new sprite from texture
    this._sprite = new PIXI.AnimatedSprite([texture1, texture2]);
    this._sprite.animationSpeed = this._speed / 12;
    this._sprite.play();

    // set position x position according to side
    if (side === 1) {
      this._sprite.x = -128; // left
    } else {
      this._sprite.x = GAME_WIDTH + 128; // right
    }

    // set slightly randomized y position
    // between -5 and 5
    const randOffset = Math.round((Math.random() * 20 - 10));
    this._sprite.y = 340 + randOffset;

    // make sure zombies overlap properly
    this._sprite.zIndex = randOffset;

    // set scale and anchor
    this._sprite.scale.x = side;
    this._sprite.anchor.set(0.5);

    // hover effect
    this._sprite.cursor = 'move';

    // interactivity, dies on click
    this._sprite.interactive = true;
    this._sprite.on('mousedown', () => {
      this._game.shoot(this._sprite.x);
      this.die();
    });

    // add color matrix for random color swap
    const colorMatrix = new PIXI.filters.ColorMatrixFilter();
    this._sprite.filters = [colorMatrix];
    colorMatrix.hue((360 * Math.random()), false);

    // add sprite to middleground
    this._game.middlegroundLayer.addChild(this._sprite);

    // add shadow
    this._shadow = new PIXI.Graphics();
    this._shadow.beginFill(0x000000, 0.08);
    this._shadow.drawEllipse(this._sprite.x, this._sprite.y + this._sprite.height / 2, 40, 12);
    this._shadow.endFill();

    // add sprite to background
    this._game.backgroundLayer.addChild(this._shadow);
  }

  get dead() {
    return this._dead;
  }

  set dead(isDead) {
    this._dead = isDead;
  }

  get sprite() {
    return this._sprite;
  }

  die() {
    this._dead = true;
    this._game.playSound(KILL_OGG);
  }

  move(deltaTime) {
    const offset = deltaTime * this._speed * this._side;
    this._sprite.x += offset;
    this._shadow.x += offset;
  }

  destroy() {
    this._sprite.destroy();
    this._shadow.destroy();
  }
}