class Rabbit {
  // game
  game;

  // sprite
  sprite;
  colorMatrix;
  shadow;
  
  // position
  defaultPositionX;
  offset = 0;
  maxOffset = 5;
  minOffset = -5;
  direction = 2;
  
  angle = 0;

  // health
  defaultHunger = 60;
  defaultThirst = 30;
  defaultStress = 45;

  maxHunger = this.defaultHunger * 2;
  maxThirst = this.defaultThirst * 2;
  maxStress = this.defaultStress * 2;
  
  hunger = this.defaultHunger;
  thirst = this.defaultThirst;
  stress = this.defaultStress;
  
  hungerIncrease = this.defaultHunger / 2;
  thirstIncrease = this.defaultThirst / 2;
  stressIncrease = this.defaultStress / 2;

  alive = true;
  causeOfDeath = 'UNKNOWN';

  constructor(game, texture) {
    this.game = game;

    // sprite and sound
    this.sprite = new PIXI.Sprite(texture);
    
    // position and anchor
    this.defaultPositionX = GAME_WIDTH / 2;
    this.sprite.x = this.defaultPositionX;
    this.sprite.y = 340;
    this.sprite.anchor.set(0.5);
    this.sprite.zIndex = -100;

    // cursor
    this.sprite.cursor = 'move';

    // interactivity, dies on click
    // do not set interactive before the game starts
    this.sprite.on('mousedown', () => {
      this.game.shoot(this.sprite.x);
      this.die('BETRAYAL');
    });

    // add color matrix for effects
    this.colorMatrix = new PIXI.filters.ColorMatrixFilter();
    this.sprite.filters = [this.colorMatrix];

    this.game.middlegroundLayer.addChild(this.sprite);

    // add shadow
    const shadowGraphics = new PIXI.Graphics();
    shadowGraphics.beginFill(0x000000, 0.08);
    shadowGraphics.drawEllipse(0, 0, 60, 20);
    shadowGraphics.endFill();

    // has to be a sprite for scaling etc
    const shadowTexture = this.game.renderer.generateTexture(shadowGraphics);
    this.shadow = new PIXI.Sprite(shadowTexture);
    this.shadow.x = this.sprite.x;
    this.shadow.y = this.sprite.y + this.sprite.height / 2
    this.shadow.anchor.set(0.5);

    // add sprite to background
    this.game.backgroundLayer.addChild(this.shadow);
  }

  get sprite() {
    return this.sprite;
  }

  refresh(deltaTime) {
    if (!this.alive) {
      return;
    }

    // hunger
    this.hunger -= deltaTime * 1 / 120;
    if (this.hunger <= 0) {
      this.die('STARVATION');
      return;
    }
    const scaleX = Math.max(0.2, this.hunger / this.defaultHunger);
    const scaleY = Math.pow(this.hunger / this.defaultHunger, 1 / 10);
    this.sprite.scale.x = scaleX;
    this.sprite.scale.y = scaleY;
    this.shadow.scale.x = scaleX;
    this.shadow.scale.y = scaleY;

    // thirst
    this.thirst -= deltaTime * 1 / 120;
    if (this.thirst <= 0) {
      this.die('DEHYDRATION');
      return;
    }
    this.colorMatrix.saturate(Math.min(0, -1 + this.thirst / this.defaultThirst), false);
    this.colorMatrix.hue(-40 + (40 * this.thirst / this.defaultThirst), true);

    // stress
    this.stress -= deltaTime * 1 / 120;
    if (this.stress <= 0) {
      this.die('SEIZURE');
      return;
    }
    this.oscillate(deltaTime);
  }

  oscillate(deltaTime) {
    // determine direction
    if (this.offset >= this.maxOffset) {
      this.direction = -2;
    }
    if (this.offset <= this.minOffset) {
      this.direction = 2;
    }

    // translate or rotate according to stress level
    if (this.stress <= this.defaultStress) {
      // the bunny is stressed
      // update offset according to stress level
      const stressFactor = 1 - Math.sqrt(Math.min(1, this.stress / this.defaultStress), 2);
      this.offset += deltaTime * this.direction * stressFactor;
      
      // translate sprite
      this.sprite.x = this.defaultPositionX + this.offset;

      // slowly rotate back to 0 if necessary
      if (this.sprite.rotation > 0) {
        this.sprite.rotation -= Math.min(this.sprite.rotation, 1 / 1000);
      }
      if (this.sprite.rotation < 0) {
        this.sprite.rotation += Math.min(-this.sprite.rotation, 1 / 1000);
      }
    }
    if (this.stress > this.defaultStress) {
      // the bunny is relaxed
      // update offset according to stress level
      const stressFactor = Math.pow(this.stress / this.defaultStress, 5);
      this.offset += deltaTime * this.direction * stressFactor / 120;

      // rotate sprite
      this.sprite.rotation = this.offset / 30;

      // slowly move back to 0 if necessary
      if (this.sprite.x > this.defaultPositionX) {
        this.sprite.x -= Math.min(this.sprite.x - this.defaultPositionX, 1);
      }
      if (this.sprite.x < this.defaultPositionX) {
        this.sprite.x += Math.min(this.defaultPositionX - this.sprite.x, 1);
      }
    }
    
    // don't forget the shadow
    this.shadow.x = this.sprite.x;  
  }

  die(cause) {
    // health
    this.alive = false;
    
    // display
    this.causeOfDeath = cause;
    this.sprite.visible = false;
    this.shadow.visible = false;

    // sound
    this.game.playSound(OUCH_OGG);
  }

  reset() {
    // health
    this.alive = true;
    this.hunger = this.defaultHunger;
    this.thirst = this.defaultThirst;
    this.stress = this.defaultStress; 

    // display
    this.causeOfDeath = 'UNKNOWN';
    this.sprite.visible = true;
    this.shadow.visible = true;

    // make interactive
    this.sprite.interactive = true;
  }

  giveFood() {
    this.hunger += this.hungerIncrease;
    if (this.hunger > this.maxHunger) {
      this.die('CARDIAC ARREST');
    } 
    else {
      this.game.playSound(FOOD_OGG);
    }
  }

  giveWater() {
    this.thirst += this.thirstIncrease;
    if (this.thirst > this.maxThirst) {
      this.die('DROWNING');
    } 
    else {
      this.game.playSound(WATER_OGG);
    }
  }

  givePet() {
    this.stress += this.stressIncrease;
    if (this.stress > this.maxStress) {
      this.die('SMOTHERING');
    } 
    else {
      this.game.playSound(STRESS_OGG);
    }
  }
}