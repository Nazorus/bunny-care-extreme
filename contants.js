// canvas
const GAME_WIDTH = 1024;
const GAME_HEIGHT = 480;

const BACKGROUND_COLOR = 0x082460;
const LOST_BACKGROUND_COLOR = 0xec1c24;
const FLOOR_COLOR = 0x321600;

// game speed
const GAME_SPEED = 2.4;

// gui images
const BUTTON_PNG = "assets/button.png";
const CROSSHAIR_PNG = "assets/crosshair.png";

const FOOD_PNG = "assets/food.png";
const WATER_PNG = "assets/water.png";
const PET_PNG = "assets/pet.png";

// actor images
const RABBIT_PNG = "assets/rabbit.png";
const ZOMBIE1_PNG = "assets/zombie1.png";
const ZOMBIE2_PNG = "assets/zombie2.png";
const HAND_PNG = "assets/hand.png";
const GUN_PNG = "assets/gun.png";
const SHOOTING_PNG = "assets/gun_shooting.png";

// sound effects
const SHOT_OGG = "assets/sound/shot.ogg";
const OUCH_OGG = "assets/sound/ouch.ogg";
const KILL_OGG = "assets/sound/kill.ogg";

const ZOMBIE1_OGG = "assets/sound/zombie1.ogg";
const ZOMBIE2_OGG = "assets/sound/zombie2.ogg";

const FOOD_OGG = "assets/sound/food.ogg";
const WATER_OGG = "assets/sound/water.ogg";
const STRESS_OGG = "assets/sound/stress.ogg";

// music
const BASS_OGG = "assets/sound/bass_loop.ogg";
const RIFF_OGG = "assets/sound/riff_loop.ogg";
const FULL_OGG = "assets/sound/full_loop.ogg";
const END_OGG = "assets/sound/end.ogg";